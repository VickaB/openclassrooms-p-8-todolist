# ToDoList

## Audit & Improve an existing project

https://openclassrooms.com/projects/ameliorer-un-projet-existant-1


### Prerequisite

For install, you will need (presuming this is a local Windows 10 install):

*   [Wamp](https://www.wampserver.com/) with php8 .
    
     (
    if Debian/Ubuntu you will need [Php8](https://www.php.net/manual/fr/intro-whatis.php), [Apache](https://httpd.apache.org/), [mariaDB](https://mariadb.org/) and [phpMyAdmin](https://www.phpmyadmin.net/).
    
    [Here](https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-20-04-fr) 
    you will find a tutorial for the whole installation
    )
*   [Composer](https://getcomposer.org/).
*   [Symfony Cli](https://symfony.com/download)

If you go local you would certainly love to use the symfony server to test project by running `symfony serve` . This command uses the Symfony Cli

You can also choose to use [Docker](https://www.docker.com/) with a php8-apache container and a mariadb container. 

You will need to set up a Dockerfile, and a docker-compose.yml file.

### Installation and Configuration

1.   Download zip or clone [repository](https://gitlab.com/oc-da-php-symfony-projects/p8-todolist)
2.   Enter database configuration in .env file at project's root. Don't forget to enter DB server version.
3.   Run `composer install` to get the dependencies installed.
            
    This last one works only if you have composer installed globally.

    If not there is a composer.phar file at project's root that you can use by running `php composer.phar install` .
4.  Run
     
     `php bin/console doctrine:database:create`
     
     `php bin/console doctrine:schema:create`
     
     `php bin/console doctrine:fixtures:load`
     
     `php bin/console cache:clear`

     Database will be created,so will the schema, and you'll get some demo fixtures loaded too.

    If you missed something just delete vendors and DB and try to install again. 
    
Hurray, you're done!

Now go to http://your/domaine, you should see your app running.

You can connect with: 

> Username: TdlAdmin
>
> Password: TdlAdmin 

(Powerful don't you think? :-D )

Contribution guide [here](../master/P8_virginie_botiba_WHITEBOOK.md)

### Thanks

Thanks to [OpenClassrooms](https://openclassrooms.com/fr/) for giving anybody, anywhere, some learning opportunities.

Thanks to my Mentor Thomas Gérault for his kind but demanding leading.

And many thanks to my Boss at work for letting me go back to School!

Let me learn more!

Love it!

