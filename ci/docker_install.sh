#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install -yqq locales apt-utils git libzip-dev libonig-dev
# Install PHP extensions
docker-php-ext-install curl zip bz2
# Install & enable Xdebug for code coverage reports
pecl install xdebug
docker-php-ext-enable xdebug
# Install and run Composer
curl -sS https://getcomposer.org/installer | php

php composer.phar install
