<?php

namespace App\Tests\Unit\Security;

use App\Entity\User;
use App\Security\LoginFormAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class LoginFormAuthenticatorTest extends TestCase
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = "app_login";
    public const PROVIDER_KEY = "main";
    protected LoginFormAuthenticator $authenticator;
    protected EntityManagerInterface | MockObject $entityManagerInterface;
    protected UrlGeneratorInterface | MockObject $urlGeneratorInterface;
    protected CsrfTokenManagerInterface | MockObject $csrfTokenManagerInterface;
    protected PasswordEncoderInterface | MockObject $passwordEncoderInterface;
    private MockObject|UserProviderInterface $userProviderInterface;

    protected function setUp(): void
    {
        $this->entityManagerInterface = $this->createMock(EntityManagerInterface::class);
        $this->urlGeneratorInterface = $this->createMock(UrlGeneratorInterface::class);
        $this->csrfTokenManagerInterface = $this->createMock(CsrfTokenManagerInterface::class);
        $this->passwordEncoderInterface = $this->createMock(UserPasswordEncoderInterface::class);
        $this->userProviderInterface = $this->createMock(UserProviderInterface::class);
        $this->authenticator = new LoginFormAuthenticator(
            $this->entityManagerInterface,
            $this->urlGeneratorInterface,
            $this->csrfTokenManagerInterface,
            $this->passwordEncoderInterface
        );
    }
    protected function getUser()
    {
        return (new User())
            ->setUsername('vicka')
            ->setPassword('banana')
            ->setEmail('vicka@banana.com');
    }

    public function testConstruct()
    {
        static::assertObjectHasAttribute(
            'entityManager',
            $this->authenticator
        );
        static::assertObjectHasAttribute(
            'urlGenerator',
            $this->authenticator
        );
        static::assertObjectHasAttribute(
            'csrfTokenManager',
            $this->authenticator
        );
        static::assertObjectHasAttribute(
            'passwordEncoder',
            $this->authenticator
        );
    }

    /**
     * @dataProvider request
     */
    public function testSupports(Request $request): void
    {
        static::assertTrue($this->authenticator::LOGIN_ROUTE === $request->attributes->get('_route')
        && $request->isMethod('POST'));
        self::assertTrue($this->authenticator->supports($request));
//        return $this->authenticator->supports($request);
    }

    /**
     * @dataProvider request
     */
    public function testGetCredentials(Request $request): void
    {
        $credentials = $this->authenticator->getCredentials($request);
        static::assertSame('VickaB', $credentials['username']);
        static::assertSame('banana', $credentials['password']);
        static::assertSame('test_token', $credentials['csrf_token']);

        $request->getSession()
            ->set(
                Security::LAST_USERNAME,
                $credentials['username']
            )
        ;
        static::assertSame(
            $request->getSession()
                ->get(Security::LAST_USERNAME),
            'VickaB'
        );
    }

    /**
     * @dataProvider credentials
     */
    public function testGetUserWithInvalidTokenException($credentials): void
    {
        $this->expectException(InvalidCsrfTokenException::class);
        $userProviderInterface = $this->createMock(UserProviderInterface::class);
        $this->authenticator->getUser($credentials, $userProviderInterface);
    }

    /**
     * @dataProvider credentials
     */
    public function testGetPassword($credentials): void
    {
        static::assertSame('banana', $this->authenticator->getPassword($credentials));
    }

    /**
     * @dataProvider request
     */
    public function testOnAuthenticationSuccessWithTargetPath(Request $request): void
    {
        $token = $this->createMock(TokenInterface::class);
        $token
            ->expects($this->any())
            ->method('getUser')
            ->willReturn(self::getUser());
        $this->assertSame(
            'tasks',
            $this->authenticator->onAuthenticationSuccess(
                $request,
                $token,
                self::PROVIDER_KEY
            )
                ->getTargetUrl()
        );
    }

    public function request()
    {
        $request = new Request();
        $session = new Session(new MockArraySessionStorage());
        $request->setMethod('POST');
        $request->request->add([
            'username' => 'VickaB',
            'password' => 'banana',
            '_csrf_token' => 'test_token',
        ]);
        $request->setSession($session);
        $request->attributes->set('_route', 'app_login');
        $request->getSession()->set('_security.main.target_path', 'tasks');

        return [
            [
                $request,
            ],
        ];
    }

    public function credentials(): array
    {

        $credentials =  [
            'username' => 'VickaB',
            'password' => 'banana',
            'csrf_token' => 'test_token',
        ];
        return [
            [
                $credentials,
            ],
        ];
    }
}
