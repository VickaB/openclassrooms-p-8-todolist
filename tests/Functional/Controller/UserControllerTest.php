<?php

namespace App\Tests\Functional\Controller;

use App\DataFixtures\AppFixtures;
use App\Entity\User;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    use FixturesTrait;

    private KernelBrowser $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = static::createClient();
        $this->loadFixtures([
            AppFixtures::class,
        ]);
    }

    public function testListActionWhileNotLoggedIn(): void
    {
        $this->client->request('GET', '/users');
        $this->assertResponseRedirects('/login', 302);
    }

    public function testListActionWhileLoggedInWithRoleUser(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneBy(['username' => 'laura']);
        $this->client->loginUser($testUser);
        $this->client->request('GET', '/users');
        $this->assertResponseStatusCodeSame(403, 'User tried to access Users list without proper authorization');
    }

    public function testListActionWhileLoggedInWithRoleAdmin(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $testUser = $userRepository->findOneBy(['username' => 'TdlAdmin']);
        $this->client->loginUser($testUser);
        $this->client->request('GET', '/users');
        $this->assertResponseIsSuccessful();
    }

    public function testCreateActionWhileNotLoggedIn(): void
    {
        $this->client->request('GET', '/users/create');
        $this->assertResponseRedirects('/login', 302);
    }

    public function testCreateActionWhileNotGrantedUserManage(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        // retrieve the test user
        $testUser = $userRepository->findOneBy(['username' => 'laura']);
        $this->client->loginUser($testUser);
        $this->client->request('GET', '/users/create');
        $this->assertResponseStatusCodeSame(403, 'User tried to create User without proper authorization');
    }

    public function testCreateActionWhileGrantedUserManage(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['username' => 'TdlAdmin']);
        $this->client->loginUser($user);
        $this->client->request('GET', '/users/create');
        $this->client->submitForm('Ajouter', [
            'user[username]' => 'TestUserAdmin',
            'user[password][first]' => 'TestUserAdmin',
            'user[password][second]' => 'TestUserAdmin',
            'user[email]' => 'testuseradmin@email.com',
            'user[admin]' => '1',
        ], 'POST');
        $this->assertResponseRedirects('/users', 302);
        $this->client->request('GET', '/users/create');
        $this->client->submitForm('Ajouter', [
            'user[username]' => 'TestUser',
            'user[password][first]' => 'TestUser',
            'user[password][second]' => 'TestUser',
            'user[email]' => 'testuser@email.com',
            'user[admin]' => '0',
        ], 'POST');
        $this->assertResponseRedirects('/users', 302);
    }

    public function testEditActionWhileNotLoggedIn(): void
    {
        $this->client->request('GET', '/users/3/edit');
        $this->assertResponseRedirects('/login', 302);
    }

    public function testEditActionWhileNotGrantedUserManage(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['username' => 'laura']);
        $this->client->loginUser($user);
        $this->client->request('GET', '/users/3/edit');

        $this->assertResponseStatusCodeSame(403, 'User tried to edit User without proper authorization');
    }

    public function testEditActionWhileGrantedUserManage(): void
    {
        $userRepository = static::$container->get(UserRepository::class);
        /**
         * @var User $testUser
         */
        $testUser = $userRepository->find(3);
        $user = $userRepository->findOneBy(['username' => 'TdlAdmin']);
        $this->client->loginUser($user);
        $crawler = $this->client->request('GET', '/users/3/edit');
        $form = $crawler->selectButton('Modifier')->form();
        $this->assertTrue($form['user[username]']->getValue() == $testUser->getUsername());
        $this->assertTrue($form['user[email]']->getValue() == $testUser->getEmail());
        $this->client->submit($form, [
            'user[username]' => 'TestUserEditAdmin',
            'user[email]' => 'testusereditadmin@email.com',
            'user[admin]' => '1',
        ], );
        $this->assertResponseRedirects('/users', 302);
        $testUser = $userRepository->find(3);
        $crawler = $this->client->request('GET', '/users/3/edit');
        $form = $crawler->selectButton('Modifier')->form();
        $this->assertTrue($form['user[username]']->getValue() == $testUser->getUsername());
        $this->assertTrue($form['user[email]']->getValue() == $testUser->getEmail());
        $this->client->submit($form, [
            'user[username]' => 'TestUserEdit',
            'user[email]' => 'testuseredit@email.com',
            'user[admin]' => '0',
        ], );
        $this->assertResponseRedirects('/users', 302);
    }
}
