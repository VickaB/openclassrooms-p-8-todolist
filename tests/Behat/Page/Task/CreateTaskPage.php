<?php

namespace App\Tests\Behat\Page\Task;

use FriendsOfBehat\PageObjectExtension\Page\SymfonyPage;

class CreateTaskPage extends SymfonyPage
{
    public function getRouteName(): string
    {
        return 'task_create';
    }

    public function create($title, $content)
    {
        $this->open();
        $this->getDocument()->fillField('task[title]', $title);
        $this->getDocument()->fillField('task[content]', $content);
        $this->getDocument()->pressButton('Ajouter');
    }
}
