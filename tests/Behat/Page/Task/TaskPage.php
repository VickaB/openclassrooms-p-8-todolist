<?php

namespace App\Tests\Behat\Page\Task;

use FriendsOfBehat\PageObjectExtension\Page\SymfonyPage;

class TaskPage extends SymfonyPage
{
    public function getRouteName(): string
    {
        return 'task';
    }
}
