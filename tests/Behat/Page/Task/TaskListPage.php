<?php

namespace App\Tests\Behat\Page\Task;

use FriendsOfBehat\PageObjectExtension\Page\SymfonyPage;

class TaskListPage extends SymfonyPage
{
    public function getRouteName(): string
    {
        return 'task_list';
    }
}
