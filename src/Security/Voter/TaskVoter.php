<?php

namespace App\Security\Voter;

use App\Entity\Task;
use App\Entity\User;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

use function in_array;

class TaskVoter extends Voter
{
    private Security $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    #[Pure]
    protected function supports($attribute, $subject): bool
    {
        return
            !empty($subject)
                ? in_array($attribute, ['TASK', 'TASK_EDIT'], true) && $subject instanceof Task
                : in_array($attribute, ['TASK', 'TASK_EDIT'], true);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
// if the user is anonymous, do not grant access
        if (!$this->security->isGranted('IS_AUTHENTICATED_FULLY', $user)) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'TASK_EDIT':
                // logic to determine if the user can EDIT

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         /**
                 * @var Task $subject
                 */
                if ($this->security->isGranted('ROLE_ADMIN')) {
                    return true;
                }
                /**
                 * @var User $user
                 */
                if ($subject->getUser() === $user) {
                    return true;
                }

                break;
            case 'TASK':
                // logic to determine if the user can VIEW

                if ($this->security->isGranted('ROLE_USER')) {
                    return true;
                }
        }
        return false;
    }
}
