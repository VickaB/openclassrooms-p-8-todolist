<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

use function in_array;

/**
 * @ORM\Table("user")
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class User implements UserInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;
/**
     * @ORM\Column(type="string", length=25, unique=true)
     * @Assert\NotBlank(groups={"registration"} , message="Vous devez saisir un nom d'utilisateur.")
     */
    private string $username;
/**
     * @ORM\Column(type="string", length=64)
     * @Assert\NotBlank(groups={"registration"}, message="Vous devez saisir un mot de passe.")
     * @Assert\Length(min=7, groups={"registration"})
     */
    private string $password;
/**
     * @ORM\Column(type="string",     length=60, unique=true)
     * @Assert\NotBlank(groups={"registration"}, message="Vous devez saisir une adresse email.")
     * @Assert\Email(groups={"registration"},message="Le format de l'adresse n'est pas correcte.")
     */
    private string $email;
/**
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="user")
     */
    private ?Collection $tasks;
/**
     * @ORM\Column(type="json")
     */
    private ?array $roles = [];
    #[Pure]
    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername($username): self
    {
        $this->username = $username;
        return $this;
    }

    public function getTasks(): ?Collection
    {
        return $this->tasks;
    }

    public function addTask($task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->setUser($this);
        }

        return $this;
    }

    public function removeTask($task): self
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
        }

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword($password): self
    {
        $this->password = $password;
        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail($email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): ?array
    {
        $roles = $this->roles;
// guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';
        return array_unique($roles);
    }

    public function setRoles(?array $roles): self
    {
        foreach ($roles as $role) {
            if (!in_array($role, $this->roles, true)) {
                $this->roles[] = $role;
            }
        }

        return $this;
    }

    public function removeRole($role): self
    {
        unset($this->roles[array_search($role, $this->roles, true)]);
        return $this;
    }
    public function getSalt(): null|string
    {
        return null;
    }
    public function eraseCredentials(): null|string
    {
        return null;
    }
}
