Feature: Task
  In order to organize my project
  As a ToDoer
  I need to add/see/edit*/delete* Task (*Task I've created only) in the ToDoList app

  Background:
    Given the following people exist:
      | username  | email           | role | password |
      | Aslak     | aslak@email.com | 0    | Aslak    |
      | Joe       | joe@email.com   | 0    | Joe      |
      | Sara      | sara@email.org  | 1    | Sara     |
    Given the following tasks exist:
      | title  | content           | user      |
      | Setup app     | blaablabla | Aslak     |
      | Clean DB      | blaablabla |           |
      | Call Customer | blaablabla | Sara      |

  Scenario: User Aslak add Task "New Task"
    Given user log in as "Aslak" with "Aslak"
#    Given user is on TaskListPage
#    When user click add task link
#    Then user should be on CreateTaskPage
#    Then user should see creation form
#    When user create Task with "Setup app" and "Setup app blaablabla"
#    Then user should see "La tâche a été bien été ajoutée"
#    Then user should be on TaskListPage

  Scenario: User Aslak tries to edit Task "Clean DB"
    Given user log in as "Aslak" with "Aslak"
    Given user is on TaskListPage
#    When user click edit :task link
#    Then user should be on Task :task EditPage

  Scenario: User Aslak tries to edit Task "Call Customer"
    Given user log in as "Aslak" with "Aslak"
    Given user is on TaskListPage
#    When user click edit :task link
#    Then user should be on Task :task EditPage

  Scenario: User Aslak tries to edit Task "Setup app"
    Given user log in as "Aslak" with "Aslak"
    Given user is on TaskListPage
#    When user click edit "Setup app" link
#    Then user should be on Task :Setup app EditPage